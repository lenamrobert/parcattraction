import { AttractionService } from '../Service/attraction.service';
import { CritiqueService } from '../Service/critique.service';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { AttractionInterface } from '../Interface/attraction.interface';
import { MatCardModule } from '@angular/material/card';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CritiqueInterface } from '../Interface/critique.interface';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-attraction',
  standalone: true,
  imports: [CommonModule, MatCardModule, ReactiveFormsModule],
  templateUrl: './attraction.component.html',
  styleUrl: './attraction.component.scss'
})
export class AttractionComponent implements OnInit {
  public attractionId: number | undefined;
  public attraction : Observable<AttractionInterface> | undefined;
  public critiques : Observable<CritiqueInterface[]> | undefined;

  public formulaireCritiques: FormGroup[] = [];

  constructor(
    private route: ActivatedRoute,
    private attractionService: AttractionService,
    private critiqueService: CritiqueService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.attractionId = Number(this.route.snapshot.paramMap.get('id'));
    this.attraction = this.attractionService.getAttractionById(this.attractionId);
    this.critiques = this.critiqueService.getCritiqueByAttractionId(this.attractionId);
  }

  public onSubmit(critiqueFormulaire: FormGroup) {
    console.log(critiqueFormulaire)
    this.critiqueService.postCritique(critiqueFormulaire.getRawValue()).subscribe(result => {
      critiqueFormulaire.patchValue({critique_id: result.result});
      console.log(result);
      this._snackBar.open(result.message, undefined, {
        duration: 1000
      });
    });
  }

  public addCritique() {
    this.formulaireCritiques.push(
      new FormGroup({
        critique_id: new FormControl(),
        attraction_id: new FormControl(this.attractionId),
        nom: new FormControl("", [Validators.required]),
        prenom: new FormControl("", [Validators.required]),
        note: new FormControl("", [Validators.required]),
        texte: new FormControl("", [Validators.required]),
        anonyme: new FormControl(0)
      }),
    );}
}
