export interface CritiqueInterface {
  critique_id: number;
  texte: string;
  note?: number;
  nom?: string;
  prenom?: string;
  anonyme?: boolean;
}