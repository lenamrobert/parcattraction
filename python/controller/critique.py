import request.request as req

def add_Critique(data):
  print(data, flush=True)
  query = "INSERT INTO critique (texte, note, nom, prenom, anonyme, attraction_id) VALUES (?, ?, ?, ?, ?, ?)"
  values = (data['texte'], data['note'], data['nom'], data['prenom'], data['anonyme'], data['attraction_id'])
  req.insert_into_db(query, values)

  return req.last_inserted_id()

def get_all_id():
  json = req.select_from_db("SELECT * FROM id")
  
  return json

def get_id(id):
  if (not id):
    return False

  json = req.select_from_db("SELECT * FROM id WHERE id = ?", (id,))

  if len(json) > 0:
    return json[0]
  else:
    return []

def delete_id(id):
  if (not id):
    return False

  req.delete_from_db("DELETE FROM id WHERE id = ?", (id,))

  return True

def get_all_critique_by_attraction_id(attraction_id):
  if (not attraction_id):
    return False

  json = req.select_from_db("SELECT * FROM critique WHERE attraction_id = ?", (attraction_id,))

  return json

def get_all_critique():
  json = req.select_from_db("SELECT * FROM critique")

  return json
