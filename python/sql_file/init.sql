DROP TABLE IF EXISTS critique;

DROP TABLE IF EXISTS attraction;

CREATE TABLE attraction (
    attraction_id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    difficulte INT,
    visible BOOL DEFAULT TRUE,
    note FLOAT DEFAULT -1.0
);

DROP TABLE IF EXISTS users;

CREATE TABLE users (
    users_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE critique (
    critique_id INT AUTO_INCREMENT PRIMARY KEY,
    texte VARCHAR(255) NOT NULL,
    note INT,
    nom VARCHAR(255),
    prenom VARCHAR(255),
    anonyme BOOL DEFAULT FALSE,
    attraction_id INT,
    FOREIGN KEY(attraction_id) REFERENCES attraction(attraction_id)
);