INSERT INTO attraction (nom, description, difficulte, visible, note) VALUES ('Silver Star', 'Montagne russe', 3, 1,3);
INSERT INTO attraction (nom, description, difficulte, visible, note) VALUES ('Montagne 8', 'Montagne russe', 4, 1,3);

INSERT INTO critique (texte, note, nom, prenom, anonyme, attraction_id) VALUES ('Super', 5, 'toto', 'toto', 0, 1);
INSERT INTO critique (texte, note, nom, prenom, anonyme, attraction_id) VALUES ('Nul!!', 1, 'toto', 'toto', 0, 1);
INSERT INTO critique (texte, note, nom, prenom, anonyme, attraction_id) VALUES ('Bien!', 5, 'toto', 'toto', 0, 2);


INSERT INTO users (name, password) VALUES ('toto', 'toto');